package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServiciosService {

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(cs).retryWrites(true).build();
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }

    public static List getAll() {
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();

        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }

        return list;
    }

    public static void insert(String cadenaServicio) throws Exception {
        Document doc = Document.parse(cadenaServicio);
        MongoCollection<Document> servicios = getServiciosCollection();

        servicios.insertOne(doc);
    }

    public static void insertBatch(String cadenaServicio) throws Exception {
        Document doc = Document.parse(cadenaServicio);
        List<Document> listServicios = doc.getList("servicios", Document.class);

        MongoCollection<Document> servicios = getServiciosCollection();

        servicios.insertMany(listServicios);
    }

    public static List<Document> getFiltrados(String cadenaFiltro) {
        MongoCollection<Document> servicios = getServiciosCollection();

        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);

        FindIterable<Document> iterDoc = servicios.find(docFiltro);

        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lista.add(it.next());
        }

        return lista;
    }

    public static List<Document> getFiltradosPeriodo(String filtroPeriodo) {
        MongoCollection<Document> servicios = getServiciosCollection();

        List lista = new ArrayList();
        Document docFiltro = new Document();
        docFiltro.append("disponibilidad.periodos", filtroPeriodo);

        FindIterable<Document> iterDoc = servicios.find(docFiltro);

        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lista.add(it.next());
        }

        return lista;
    }

    public static void update(String cadena) throws Exception {

        JSONObject jsonObj = new JSONObject(cadena);

        String filtro = jsonObj.getJSONObject("filtro").toString();
        String valores = jsonObj.getJSONObject("valores").toString();

        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);

        servicios.updateOne(docFiltro, docValores);
    }
}
