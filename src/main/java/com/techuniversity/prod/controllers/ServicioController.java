package com.techuniversity.prod.controllers;

import com.techuniversity.prod.servicios.ServiciosService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {

    /*
    @GetMapping("/servicios")
    public List getServicios() {
        return ServiciosService.getAll();
    }
    */

    @PostMapping("/servicio")
    public String insertServicio(@RequestBody String cadena) {
        try {
            ServiciosService.insert(cadena);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @PostMapping("/servicios")
    public String insertServicios(@RequestBody String cadena) {
        try {
            ServiciosService.insertBatch(cadena);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServiciosService.getFiltrados(filtro);
    }

    @GetMapping("/servicios/periodo")
    public List getServiciosFiltro(@RequestParam String periodoId) {
        return ServiciosService.getFiltradosPeriodo(periodoId);
    }

    @PutMapping("/servicios")
    public String updateServicios(@RequestBody String data) {
        try {
            ServiciosService.update(data);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
