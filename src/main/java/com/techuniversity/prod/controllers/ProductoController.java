package com.techuniversity.prod.controllers;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(@RequestParam(defaultValue = "-1") String page) {
        int iPage = Integer.parseInt(page);

        if (iPage == -1) {
            return productoService.findAll();
        }

        return productoService.findPaginado(iPage);
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProducto(@PathVariable String id) {
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel insertProducto(@RequestBody ProductoModel productoModel) {
        return productoService.save(productoModel);
    }

    @PutMapping("/productos")
    public void updateProducto(@RequestBody ProductoModel productoModel) {
         productoService.save(productoModel);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoModel) {
        return productoService.delete(productoModel);
    }
}
