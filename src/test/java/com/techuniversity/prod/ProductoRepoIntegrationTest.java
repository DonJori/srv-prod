package com.techuniversity.prod;
;
import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepository;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ProductoRepoIntegrationTest {
    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll() {
        List<ProductoModel> productos = productoRepository.findAll();
        assertTrue(productos.size() > 0);
    }

    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
                crearUrlConPuerto("/productos/productos/60aca97c78759c1b5c630f9d"),
                HttpMethod.GET,
                entity,
                String.class);
        String expected = "{\"id\":\"60aca97c78759c1b5c630f9d\","
                + "\"nombre\":\"Producto 1\","
                + "\"descripcion\":\"Primer producto de risas modificado\","
                + "\"precio\": 121.0}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    private String crearUrlConPuerto(String url) {
        String urlCompleta = "http://" + "localhost" + ":" + port + url;
        System.out.println(urlCompleta);
        return urlCompleta;
    }
}
